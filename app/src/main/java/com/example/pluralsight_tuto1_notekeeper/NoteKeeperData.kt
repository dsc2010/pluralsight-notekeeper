package com.example.pluralsight_tuto1_notekeeper

/**
 * Data Class to store information about a course.
 * @param courseId: The id of the course
 * @param title: The Course's title
 */
data class CourseInfo (val courseId: String, val title: String) {
    override fun toString(): String {
        return title
    }
}

/**
 * Data Class holding notes relative to a course.
 * @param course: The course for which the note is being written
 * @param title: Title of the note
 * @param text: The notes
 */
data class NoteInfo(var course: CourseInfo? = null, var title: String? = null, var text: String? = null) {

}