package com.example.pluralsight_tuto1_notekeeper

/**
 * Class to manage data within the app.
 */
object DataManager {
    val courses = HashMap<String, CourseInfo>()
    val notes = ArrayList<NoteInfo>()

    init {
        initializeCourses()
        initializeNotes()
    }

    /**
     * Populates course list {@link courses}
     */
    private fun initializeCourses() {
        var course = CourseInfo("android_intents", "Android Programming with Intents")
        courses.set(course.courseId, course)

         course = CourseInfo("android_async", "Android Async Programming and Services")
        courses.set(course.courseId, course)

         course = CourseInfo("java_lang", "Java Fundamentals: The Java Language")
        courses.set(course.courseId, course)

         course = CourseInfo("java_core", "Java Fundamentals: The Core Platform")
        courses.set(course.courseId, course)
    }

    /**
     * Populate notes ){@link notes}
     */
    private fun initializeNotes() {
        var note = NoteInfo(course = courses.get("android_intents")!!, title="Note 1", text ="Sample note1 for android_intents")
        notes.add(note)
        note = NoteInfo(course = courses.get("android_async")!!, title="Note 2", text ="Sample note1 for android_async")
        notes.add(note)
        note = NoteInfo(course = courses.get("android_async")!!, title="Note 3", text ="Sample note2 for android_async")
        notes.add(note)
        note = NoteInfo(course = courses.get("java_lang")!!, title="Note 4", text ="Sample note1 for android_intents")
        notes.add(note)
    }
}